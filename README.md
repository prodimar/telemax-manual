# Telemax: Sistema de telecontrol

El sistema de telecontrol está constituído por dos tipos de elementos distintivos:

  - receptor `[RecMAXP]`, alimentado desde la red eléctrica, y que permite gobernar el estado de 8 salidas de relé, más 1 relé maestro que controla la alimentación hacia esas salidas.

  - emisores (mandos a distancia), alimentados mediante 2 pilas AAA. Existen 2 variantes de mando según el número de botones, `[EmiMAX8]` y `[EmiMAX14]`. La pulsación sobre los botones del mando a distancia permite actuar remotamente sobre los relés del receptor.

Ambos tipos de elementos se comunican vía radio en la banda de 868 MHz con detección rápida de desconexión (pérdida de comunicación) del mando y desactivación de seguridad de todos los relés en el receptor.

Un procedimiento de asociación previo permite configurar qué mandos se comunican con qué receptores. Un mando sólo puede estar asociado a 1 receptor simultáneamente, mientras que un receptor puede tener hasta 8 mandos asociados, pero sólo 1 en operación simultáneamente.
 

## Descripción del receptor

En la siguiente imagen se muestran los elementos constitutivos del receptor:

![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/receiver.png "Receptor RecMAXP")

El led de estado del receptor proporciona las siguientes indicaciones:

| Led                | Indicación
|:------------------:|-------------
| Apagado            | El receptor no tiene mandos asociados
| Parpadeo lento     | El receptor tiene al menos 1 mando asociado
| Encendido fijo     | Receptor en uso: mando conectado
| Parpadeo rápido    | En modo de asociación

El receptor está dotado de 8 relés más un relé maestro. Dependiendo del perfil de operación configurado en el mando, el relé maestro puede activarse:

  - permanentemente mientras el emisor permanezca conectado, o bien

  - si y sólo si alguno de los 8 relés esté activo.

Cuando se detecta la desconexión de un mando (porque éste se haya apagado o se haya perdido la conexión radio) todos los relés del receptor se desactivan automáticamente por seguridad.

En el conector de expansión de un receptor puede conectarse en cascada hasta 3 placas auxiliares adicionales, lo que permite elevar el número máximo de relés controlables hasta 32. Las electrónica de las placas auxiliares debe tener la configuración de direccionamiento del expansor E/S MCP23008 correctamente establecida, tal como se refleja en la siguiente tabla:

| #Placa             | Relés   | MCP23008: Configuración pines Ax
|:------------------:|---------|-----------------------------------------------
| RecMAXP            | 0  - 7  | A2 = GND, A1 = GND, A0 = GND
| Placa auxiliar #1  | 8  - 15 | A2 = GND, A1 = GND, A0 = VDD
| Placa auxiliar #2  | 15 - 23 | A2 = GND, A1 = VDD, A0 = GND
| Placa auxiliar #3  | 24 - 31 | A2 = GND, A1 = VDD, A0 = VDD


## Descripción del emisor

En la siguiente imagen se muestran los elementos constitutivos del frontal de los emisores EmiMAX8 / EmiMAX14:

|          |             |
|----------|-------------|
| ![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/emimax8.png "Emisor EmiMAX8") | ![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/emimax14.png "Emisor EmiMAX14") |

Los emisores EmiMAX están dotados de microswitches de configuración para la selección del perfil de funcionamiento, que se muestran en la imagen siguiente:

|          |             |
|----------|-------------|
| ![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/emimax8hw02sw.png "Microswitches EmiMAX8") | ![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/emimax14hw02sw.png "Microswitches EmiMAX14") |

El perfil seleccionado, de entre los configurados, es el siguiente, según el estado de los microswitches:

SW3 | SW2 | Sw1 | SW0 | #Perfil
----|-----|-----|-----|--------:
OFF | OFF | OFF | OFF |   0
OFF | OFF | OFF | ON  |   1
OFF | OFF | ON  | OFF |   2
OFF | OFF | ON  | ON  |   3
OFF | ON  | OFF | OFF |   4
OFF | ON  | OFF | ON  |   5
OFF | ON  | ON  | OFF |   6
OFF | ON  | ON  | ON  |   7
ON  | OFF | OFF | OFF |   8
ON  | OFF | OFF | ON  |   9
ON  | OFF | ON  | OFF |  10
ON  | OFF | ON  | ON  |  11
ON  | ON  | OFF | OFF |  12
ON  | ON  | OFF | ON  |  13
ON  | ON  | ON  | OFF |  14
ON  | ON  | ON  | ON  |  15

El cambio de perfil se hace efectivo en el siguiente encendido del mando.

La herramienta de configuración de perfiles es un software que permite cargar en un mando un nuevo conjunto de perfiles. Su operación se describe más adelante, en una sección separada.

El emisor está dotado de un led de estado bicolor (verde/rojo/naranja) que proporciona las siguientes indicaciones:

| Led                       | Indicación
|:-------------------------:|-----------
| Apagado                   | Emisor apagado
| Parpadeo lento (ambar)    | Emisor en estado de asocación
| Parpadeo lento (verde)    | Emisor asociado, intentando conectar al receptor
| Parpadeo rápido (color variable) | Emisor conectado al receptor

Cuando emisor y receptor están conectados, el led parpadea rápido (2 veces por segundo). En este caso el color del led indica el modo _shift_ (ver debajo).

Adicionalmente, cuando emisor y receptor están conectados y el usuario pulsa cualquier tecla, la frecuencia de parpadeo se doblará para señalizar la pulsación.

Un led independiente para indicación del estado de batería emite parpadeos indicando que la batería debe ser reemplazada.


### Teclas especiales / Perfiles de operación / Perfil por defecto

El perfil configurado en el mando determina el significado y operativa de cada una de las teclas. Existen 4 tipos de teclas especiales:

1. Tecla **START**: Una pulsación y posterior liberación enciende el mando. Esta tecla puede además controlar un relé.

2. Tecla **STOP**: La pulsación de esta tecla apaga el mando. Esta tecla *no* es compatible con la asignación de un relé.

3. Tecla **_Asociar_**: Si se mantiene pulsada esta tecla mientras se enciende el mando, el mando borrará la asociación anterior a un receptor, si existe, y entrará en modo de asociación a un nuevo receptor. Esta tecla es compatible con la asignación de un relé.

4. Tecla **_Shift_**: Esta tecla (opcional dependiendo del perfil) permite ciclar entre las páginas del modo _Shift_. Esta tecla *no* es compatible con la asignación de un relé.

El resto de las teclas se dedican típicamente al control de relés. El/los relé/s que activa cada tecla dependen del perfil configurado. Además, una tecla puede controlar sus relés asignados en uno de los dos siguientes modos:

- **instantáneo**: el/los relé/s permanecen activos mientras la tecla permanezca pulsada

- **con enclavamiento**: una pulsación sobre la tecla activa el/los relé/s, una nueva pulsación lo/s desactiva.

Adicionalmente puede definirse uno de los relés como *Relé de potencia*. Si se define este relé, la actuación sobre él, al ser activado o desactivado a la vez que otros relés, estará brevemente adelantada respecto al resto en la activación o brevemente retrasada en la desactivación.

Si un mando no tiene configurado ningún perfil (por ejemplo porque nunca ha sido programado), ejecutará el perfil por defecto siguiente:

```
teclaStart = 0 (arriba izquierda)
teclaStop  = 1 (arriba derecha)
teclaEmparejar = 2 (segunda fila izquierda)
teclaShift no hay
Cada tecla (a partir de la segunda fila, start y stop no) activa un relé, sin enclavamiento
El relé maestro no está siempre activo (se activa sólo al activar alguno de los otros relés)
```

### Modo _Shift_

El modo _shift_ permite controlar más relés que teclas tiene el mando. En caso de estar configurada, la tecla _Shift_ conmuta entre hasta 3 "páginas" de actuación sobre los relés. La "página" actual se indica mediante el color del led durante la conexión con el receptor (parpadeo rápido), según la siguiente tabla:

| Página      | Color de led|
|:-----------:|:-----------:|
  0 (inicial) |    Verde
       1      |    Naranja
       2      |    Rojo

En cada una de las páginas más allá de la primera (página 0), al índice de el/los relé/s activados por cada pulsación se le suma un offset configurable por el perfil.

## Procedimientos de asociación receptor - mando

### Asociación de un mando a un receptor

1. Mantener pulsado el botón del receptor durante 1 segundo y soltarlo. En este momento el led empezará a parpadear rápidamente y el receptor habrá entrado de nuevo en modo de asociación (del que saldrá pasados 30 segundos o al completar la asociación de un mando).

2. Apagar el mando a asociar, si no está todavía apagado.

3. Pulsar y mantener pulsado el botón _Asociar_ del mando (su ubicación depende del perfil configurado).

4. Encender el mando, pulsando y liberando el botón START (su ubicación depende del perfil configurado). Liberar también el botón _Asociar_.

5. En este punto, el mando iniciará y completará automáticamente el proceso de asociación con un receptor en rango que esté en modo de asociación. El éxito del proceso puede observarse porque ambos quedarán en estado conectado y por tanto el led del receptor quedará en encendido fijo y el del mando en parpadeo rápido.
	
### Borrado de todos los mandos asociados a un receptor

1. Mantener pulsado el botón del receptor durante 5 segundos, hasta que el led parpadee rápidamente. En este momento el receptor habrá entrado de nuevo en modo de asociación (del que saldrá en cualquier caso pasados 30 segundos) pero borrando previamente todos los emisores asociados.

## Especificaciones

- Tiempo de autodetección de la pérdida de conexión con el mando

	- 2 segundos

- Tiempo de autoapagado del mando por inactividad

	- 5 minutos

- Rango de comunicación radio:

	- 150 metros (con visión directa entre emisor y receptor)

- Tiempo de vida de batería (est. 1000 mAh):

    - en espera: 2 años

    - en operación: 350 horas

## Software

### Herramienta de configuración de perfiles

El software de configuración de perfiles es una herramienta de línea de comandos que permite cargar en un mando perfiles de operación. La configuración de los perfiles se realiza en un fichero de texto, según el siguiente ejemplo autoexplicativo:

```
[Simple]
; Si se define a un valor distinto de 0, el relé maestro estará activo siempre que la conexión
;  entre el mando y el receptor esté establecida. En caso contrario, el relé maestro se activará
;  solamente cuando 1 ó más de los otros relés estén activos
releMaestroSiempreActivo = 1

; Definición de un relé especial "de potencia" (comentar la línea o definir un valor -1 para no definirlo)
;  Si se define este relé, la actuación sobre él, al ser activado o desactivado a la vez que otros relés,
;  estará brevemente adelantada respecto al resto en la activación o brevemente retrasada en la desactivación
releDePotencia = -1

; La tecla SHIFT permite avanzar (en cada pulsación) hasta a un máximo de 3 "mandos virtuales".
;  Una nueva pulsación en el último "mando virtual" volverá al mando original
;  En cada uno de esos mandos, el relé activado por cada tecla es, el/los configurados individualmente
;  para la tecla + la base definida aquí.
releBaseParaModosShift = 0, 10, 20

; Identificadores de teclas especiales (comentar la línea o definir un valor -1 para no definir la tecla):
;  Start: encendido (SI es compatible con la asignación de un relé)
;  Stop: apagado (NO es compatible con la asignación de un relé)
;  Shift: avance de "mando virtual" (NO es compatible con la asignación de un relé)
;  Assoc: inicia el procedimiento de asociación si se mantiene pulsada esta tecla mientras se enciende el mando (tecla Start)
;  HombreMuerto: requiere ser pulsada para que se haga efectiva la pulsación de las teclas dependientes de ella
teclaStart = 0
teclaStop = 1
teclaShift = -1
teclaAsociar = 2
teclaHombreMuerto = 14

; El control de relés realizado por las teclas listadas en la variable siguiente opera con enclavamiento.
;  En caso contrario, la tecla opera en modo instantáneo (requiere mantenerse pulsada para activar su/s relé/s)
teclasConEnclavamiento = 3

; Lista de relés que activa la pulsación de la tecla nn-ésima (nn: 00, 01.. 10, 11, 12, 13, 14, 15)
; (si una tecla no se lista o no tiene relés asignados, entonces no activa ningún relé)
relesTecla02 = 0
relesTecla03 = 0, 1

; Si se define a un valor distinto de 0, el mando emite a potencia radio reducida, para aplicaciones de corto alcance
potenciaRfReducida = 0

; Lista de teclas que requieren la pulsación simultánea de la tecla de hombre muerto para que su pulsación se haga efectiva
teclasDependientesHombreMuerto = 2, 4

; Lista de pares de teclas (contiguos en horizontal) de actuación excluyente: si está activa una de ellas, la actuación sobre la otra
;  no se hace efectiva hasta que no se libera la primera
; (listar aquí una tecla añade automáticamente su "vecina en horizontal", por lo que basta listar 1 por fila)
; (No debe listarse la inexistente tecla 15 aquí. Si se define la tecla 15 aquí, se considera la contiguidad (arriba-abajo),
;  en vez de (izquierda-derecha), aunque en este caso es necesario listar la vecina vertical explícitamente).
;  Esto resuelve el caso específico de la exclusión en vertical de los botones 1 y 3, en firmwares EmiMAX >= 0.7
;  configurando "paresTeclasExcluyentes = 1, 3, 15")
paresTeclasExcluyentes = 6, 9

; Lista de pares de teclas (contiguos en horizontal) de activación/liberación: la tecla listada aquí enclava los relés asignados a ella,
;  mientras que su "vecina en horizontal" los desenclava. La actuación sobre la tecla de enclavar con los relés ya activos o sobre la de
;  desenclavar con los relés ya inactivos no tienen efecto
; (listar aquí una tecla añade automáticamente su "vecina en horizontal", por lo que basta listar 1 por fila, que ha de ser la de activación (y NO la de liberación))
; (listar aquí una tecla la añade automáticamente a la lista de teclasConEnclavamiento)
; (los relés asignados a cada tecla listada aquí y su "vecina en horizontal" se igualan automáticamente a la unión (U) de los relés listados para cada una de ellas,
;  para evitar inconsistencias en la configuración)
paresTeclasEnclavaLibera = 10, 13

; Si se define a un valor distinto de 0, la pulsación de cualquier tecla, excepto la de Asociar (y no sólo de la tecla Start) provoca el encendido del mando
; Sin embargo, aunque esta opción esté activada, para entrar en modo de asociación debe utilizarse la combinación teclaAsociar + teclaStart(real, no cualquiera)
; NOTA: Tras seleccionar un perfil con este modo activo, la reconfiguración mediante switches sólo puede hacerse quitando y poniendo las pilas, no mediante un apagado/encendido normal
cualquierTeclaEnciendeMando = 0

; Si se define a un valor distinto de 0, se utiliza el modo de asociación clásico en el que un mando sólo puede estar asociado a 1 único receptor
modoDeAsociacionClasico = 1

; Valor, en segundos, del temporizador de apagado automático del mando (0, deshabilitado, el mando nunca se apaga por inactividad)
temporizadorDeApagadoAuto =

; Lista de relés (0..7) cuyo valor es calculado por el receptor, no por el mando, y por tanto su estado persiste frente a desconexiones del mando
relesCalculadosEnReceptor =

; Lista de relés (0..7) cuyo valor es calculado por el receptor, no por el mando, y por tanto su estado persiste frente a desconexiones del mando
; Estos relés se controlan no mediante una tecla, sino mediante un par de teclas: una de enclavamiento y otra de liberación
; Para configurar el funcionamiento en este modo, a la tecla de enclavar se le asigna el control del relé N y a la tecla de liberar se le asigna el relé 8 + N
; NOTA: Este modo es independiente del control de relés mediante el mando mediante la configuración "paresTeclasEnclavaLibera"
; NOTA: En caso de activar este modo, se pierde la posibilidad de que el/los relés reales (8+N) puedan ser controlador con normalidad con otra tecla
; Ejemplo: Queremos controlar el relé número 2 del receptor en "modo calculado por el receptor" de tal forma que la tecla 4 del mando lo enclave y la tecla 5 lo libere.
;  Entonces configuramos: relesTecla04 = 2, relesTecla05 = 10, relesCalculadosEnReceptorConParEnclavaLibera = 2
relesCalculadosEnReceptorConParEnclavaLibera =

; IMPORTANTE: Tanto las teclas como los relés se numeran empezando en 0
```

Un nuevo ejemplo de perfil podría ser:
```
[Standard9]
releMaestroSiempreActivo = 1
releBaseParaModosShift = 0, 6, 12
teclaStart = 0
teclaStop = 1
teclaShift = 8
teclaAsociar = 2
teclasConEnclavamiento = 3, 5, 7
relesTecla02 = 0
relesTecla03 = 1
relesTecla04 = 2
relesTecla05 = 3
relesTecla06 = 4
relesTecla07 = 5
```

Un ejemplo con configuración del *Relé de potencia* sería:
```
[Cabrestante]
releMaestroSiempreActivo = 0
releDePotencia = 4
releBaseParaModosShift = 0
teclaStart = 0
teclaStop = 1
teclaShift = -1
teclaAsociar = 2
teclasConEnclavamiento =
relesTecla02 = 0, 1, 4
relesTecla03 = 2, 3, 4
```

Una vez definidos en el fichero los perfiles deseados, la asignación de perfiles a microswitches se realiza en una sección de nombre `[Perfiles]`, como en el siguiente ejemplo, en el que se asignan los 2 perfiles de ejemplo construídos a las posiciones de microswitches 0 y 15 respetivamente, y el resto de perfiles no se definen.
```
[Perfiles]
perfil00 = Simple
perfil01 = Cabrestante
perfil02 = 
perfil03 = 
perfil04 = 
perfil05 = 
perfil06 = 
perfil07 = 
perfil08 = 
perfil09 = 
perfil10 = 
perfil11 = 
perfil12 = 
perfil13 = 
perfil14 = 
perfil15 = Standard9

; IMPORTANTE: Los perfiles posibles se numeran del 00 al 15
```

#### Descarga e instalación del software

El kit de herramientas de configuración puede descargarse [aquí](https://bitbucket.org/prodimar/telemax-manual/downloads/TelemaxSoftwareKit.zip).

El kit no requiere instalación, basta descomprimir el archivo en una carpeta.

#### Uso del software

Para facilitar el uso de este software se han creado una serie de ficheros, incluídos en el kit. Los pasos para la configuración de un mando serían:

1. Preparar un fichero con los perfiles deseados y renombrarlo como `tmxconfig.ini`

2. [adaptador para micros OnSemi] Conectar el adaptador de comunicaciones al PC. Windows habrá asignado un número de puerto COM al adaptador. Para averiguarlo:
    
    1. Abrir el administrador de dispositivos (pulsar tecla Windows+X y seleccionar Administrador de dispositivos del menú emergente)

    2. En la lista de dispositivos, navegar hasta _Puertos (COM y LPT)_

    3. Bajo ese epígrafe aparecerán y desaparecerán 2 entradas _USB Serial Port (COMxx)_ al conectar y desconectar el adaptador de comunicaciones. El número que nos interesa es el de la segunda entrada.
    ![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/devicemanager.png "Determinación del puerto COM del adaptador")

    4. Si tras seguir estos pasos no se detectan los dispositivos mencionados, instalar los drivers descargables de [aquí](https://bitbucket.org/prodimar/telemax-manual/downloads/drivers_ftdi.zip).

2. [adaptador para micros EFR32] Conectar el adaptador de comunicaciones al PC. Windows habrá asignado un número de puerto COM al adaptador. Para averiguarlo:

    1. Abrir el administrador de dispositivos (pulsar tecla Windows+X y seleccionar Administrador de dispositivos del menú emergente)

    2. En la lista de dispositivos, navegar hasta _Puertos (COM y LPT)_

    3. Bajo ese epígrafe aparecerá y desaparecerá 1 entradas _JLink Uart Port (COMxx)_ al conectar y desconectar el adaptador de comunicaciones. xx es el número que nos interesa.

    4. Si tras seguir estos pasos no se detectan los dispositivos mencionados, instalar los drivers descargables en los siguientes enlaces, para las variantes de [32 bits](https://bitbucket.org/prodimar/telemax-manual/downloads/JLink_Windows_V794a_i386.exe) o [64 bits](https://bitbucket.org/prodimar/telemax-manual/downloads/JLink_Windows_V794a_x86_64.exe).

3. El kit de herramientas incluye un archivo `PuertoSerie.txt`. Escribir en ese archivo, en la primera línea, el número de puerto descubierto en el punto anterior. Guardar el archivo actualizado.

4. Conectar el adaptador de comunicaciones a la placa del mando.

5. Ejecutar el archivo `ConfigurarMandos.bat`. El proceso de configuración arrancará automáticamente. Una ejecución con éxito tiene el siguiente aspecto:
	![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/tmxconfigurator.png "Configuración de perfiles")

Con versiones recientes del kit, al finalizar la ejecución de la configuración, el software consultará y mostrará la versión de firmware del equipo. Este procediemiento también puede iniciarse de manera indepediente (imprescindible para consultar la versión de recepctor, ya que este tipo de dispositivo no admite configuración) ejecutando el archivo `ConsultarVersion.bat`. La información de versión se muestra del siguiente modo:
![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/consulta_version.png "Información de versión")


#### Procedimientos específicos para fabricación: asignación de número de serie

Para el correcto funcionamiento del sistema, todos los elementos del mismo deben tener una dirección radio distinta. Eso implica a su vez que ha de serles asignado tras su fabricación un número de serie único. 

Para facilitar este proceso, el archivo `AsignarNumerosDeSerie.bat` asigna iterativamente, a cada dispositivo conectado, el número de serie presente en el archivo `serial.txt` y, en caso de una asignación con éxito, incrementa en 1 automáticamente el número de serie presente en dicho archivo.

La siguiente captura refleja el proceso:

![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/asigna_numserie.png "Asignación de número de serie")


## Procedimiento de actualización de firmware [micros OnSemi]

El firmware de emisor y receptor pueden ser actualizados usando el mismo mecanismo de comunicación que para la configuración. Para la actualización de firmware deben seguirse los siguientes pasos:

1. Descargar el kit de software de actualización [aquí](https://bitbucket.org/prodimar/telemax-manual/downloads/TelemaxFirmwareUpdateKit.zip). El kit no requiere instalación, basta descomprimir el archivo en una carpeta.

2. Descargar la nueva imagen de firmware deseada de [aquí](#markdown-header-lista-de-firmwares-disponibles) y copiarla en la carpeta raíz del kit de software de actualización (al mismo nivel que el archivo `ActualizarFirmware.bat`) **con nombre `firmware.ihx`**.

4. Conectar el adaptador de comunicaciones a la placa a actualizar, del mismo modo que para una reconfiguración de perfiles.

5. Ejecutar el archivo `ActualizarFirmware.bat`. El proceso de actualización arrancará automáticamente. Una ejecución con éxito tiene el siguiente aspecto:
  ![alt](https://bitbucket.org/prodimar/telemax-manual/raw/master/media/firmwareupdate.png "Actualización de firmware")

   ***NOTA***: El procedimiento de `ActualizarFirmware.bat` con un archivo de firmware sin sufijo `_mnf` preserva la configuración y el número de serie del equipo, pero no es aplicable (no funcionará correctamente) si el firmware programado previamente tiene habilitado el _Watchdog_. En este caso debe utilizarse la variante `ActualizarFirmwareConBorrado.bat`, con un archivo de firmware con sufijo `_mnf` y reasignar posteriormente el número de serie del equipo y reconfigurarlo, de ser necesario.


## Procedimiento de actualización de firmware [micros EFR32]

El firmware de emisor y receptor pueden ser actualizados usando el mismo mecanismo de comunicación que para la configuración. Para la actualización de firmware deben seguirse los siguientes pasos:

1. Descargar el kit de software de actualización [aquí](https://bitbucket.org/prodimar/telemax-manual/downloads/TelemaxFirmwareUpdateKit.EFR32.zip). El kit no requiere instalación, basta descomprimir el archivo en una carpeta.

2. Descargar la nueva imagen de firmware deseada de [aquí](#markdown-header-lista-de-firmwares-disponibles) y copiarla en la carpeta raíz del kit de software de actualización (al mismo nivel que el archivo `ActualizarFirmware.bat`) **con nombre `firmware.s37`**.

4. Conectar el adaptador de comunicaciones a la placa a actualizar, del mismo modo que para una reconfiguración de perfiles.

5. Ejecutar el archivo `ActualizarFirmware.bat`.


## Lista de firmwares disponibles

### EmiMAX

* [versión 01](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20200124_0_1_1.ihx)

    - Versión inicial programada en los prototipos

* [versión 02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20200214_0_2_1.ihx)

    - Cuando no se recibe asentimiento del receptor, se reenvía un mensaje radio instantáneamente

    - La entrada en modo asociación no borra la asociación actual si no se completa una nueva

    - Corregida indicación de batería baja

* [versión 03](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20200221_0_3_1.ihx)

    - Configurabilidad del "Relé de potencia"

    - Mejora de la latencia

* [versión 04](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20200228_0_4_2.ihx)

    - Solucionado problema de bloqueo en la recepción

* [versión 05](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20200309_0_5_1_mnf.ihx)

    - Solucionada incapacidad de actualización posterior de firmware

* [versión 06 - hw02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20201023_hw02_0_6_3_mnf.ihx)

    - Para versión 02 de la PCB ("hw02"), con led de batería baja separado y microswitches en ambos mandos

    - Soporte de nuevas funcionalidades: potencia de transmisión reducida, botón de hombre muerto, pares de teclas excluyentes y de activación/liberación

    - Mejorada coexistencia con interferentes

* [versión 07 - hw02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20201028_hw02_0_7_2_mnf.ihx)

    - Solucionada desactivación de relé común en teclas instantáneas (no de enclavamiento) al despulsar una manteniendo pulsada otra

    - Solucionada selección espuria de relé 7 como relé de potencia 

    - Soporte específico para mando de 4 botones que requiere botones 1 y 3 "interlocked", siendo "vecinos" en vertical

* [versión 07/0003 - hw02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20201106_hw02_0_7_3_mnf.ihx)

    - Solucionado problema de activación simultánea de teclas excluyentes, que resultaba en la activación de ambas (ahora no se activa ninguna de ellas)

* [versión 09/0001 - hw02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20210409_hw02_0_9_1_mnf.ihx)

    - Corregida inversión de pares de teclas enclava/libera tras a una desconexión del receptor

    - Umbral de batería baja reducido de 2,5 a 2,4 V

    - Desconexión rápida del receptor cuando se apaga el mando

* [versión 10/0001 - hw02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20210420_hw02_1_0_1_mnf.ihx)

    - El temporizador de inactividad de apagado en el mando pasa a ser configurable

    - Opción de encendido con cualquier tecla (excepto tecla de asociar)

        - la/s pulsación/es que dieron lugar al encendido se procesan como teclas de acción sobre relés

        - NOTA: Tras seleccionar un perfil con este modo activo, la reconfiguración mediante switches sólo puede hacerse quitando y poniendo las pilas, no mediante un apagado/encendido normal

    - Cálculo de relés en receptor

    - Modo de asociación múltiple

        - Ya no se entra en modo de asociación automáticamente al no tener ningún receptor asociado

  * [versión 11/0001 - hw02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20210430_hw02_1_1_1_mnf.ihx)

    - Control de relés calculados por el receptor mediante par de teclas enclava/libera

    - Conmutación automática al modo de asociación clásico si el firmware del receptor no soporta el modo de asociación múltiple

  * [versión 12/0001 - hw02](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20210506_hw02_1_2_1_mnf.ihx)

     - El apagado por inactividad no señaliza al receptor la desconexión, para que éste no apague sus relés

  * [versión 12/0001 - hw04](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20220208_hw04_1_2_1_mnf.ihx)

     - Variante para versión hardware "no-SIP"

  * [versión 14/0001 - hw04](https://bitbucket.org/prodimar/telemax-manual/downloads/emimax_20220627_hw04_1_4_1_mnf.ihx)

     - Rehabilitado Watchdog

     - Remedio para error en librería "ax5043_probeirq", que provocaba el bloqueo de los mandos

### RecMAX

* [versión 01](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20200124_0_1_1.ihx)

    - Versión inicial programada en los prototipos

* [versión 02](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20200221_0_2_1.ihx)

    - Configurabilidad del "Relé de potencia"

    - Solucionado problema de bloqueo en la recepción

* [versión 03](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20200228_0_3_2.ihx)

    - Solucionado problema de bloqueo en la recepción

* [versión 04](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20200309_0_4_1_mnf.ihx)

    - Solucionada incapacidad de actualización posterior de firmware

* [versión 05](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20201023_0_5_2_mnf.ihx)

    - Soporte para hasta 3 placas auxiliares adicionales, para un máximo de 32 relés 

    - Mejorada coexistencia con interferentes

* [versión 06](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20201106_0_6_1_mnf.ihx)

    - Retardo de "relé de potencia" reducido a 50 ms

    - Reactivado watchdog, como mecanismo de seguridad para evitar el bloqueo en la recepción

* [versión 07/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20210409_0_7_1_mnf.ihx)

    - Tiempo de pulsación corta de tecla de asociación reducido de 2 a 1 s.

    - Desconexión rápida cuando se apaga el mando

* [versión 08/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20210420_0_8_1_mnf.ihx)

    - Soporte para salidas cuyo valor es calculado en el receptor (en vez de en el mando)

    - Soporte para modo de asociación múltiple por parte del mando

* [versión 09/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20210430_0_9_1_mnf.ihx)

    - Control de relés calculados por el receptor mediante par de teclas enclava/libera

    - Desactivación de relés calculados por el receptor ante la notificación de un apagado explícito desde el mando

    - Indicación al mando de soporte de modo de asociación múltiple

* [versión 10/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20210506_1_0_1_mnf.ihx)

    - Solucionado problema en asociación de mando clónico a otro ya asociado

* [versión 10/0001 - hw04](https://bitbucket.org/prodimar/telemax-manual/downloads/recmaxp_20220208_hw04_1_0_1_mnf.ihx)

    - Variante para versión hardware "no-SIP"

### RecMAXP32

(variante para versión de hardware con micro "EFR32")

* [versión 11/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/RecMAXP32_20231212_1_1_1_mnf.s37)

    - Cuando el receptor no tiene un mando asociado, da un destello breve cada 4 segundos, fácilmente distinguible del estado de con mandos asociados

    - Cuando se realiza una pulsación larga sobre el botón de forma que implicaría el borrado de los mandos asociados, se enciende el led y se mantiene encendido fijo hasta que se libera la tecla, momento en el cual se procede al borrado de los mandos asociados y a la entrada en modo aprendizaje

* [versión 11/0002](https://bitbucket.org/prodimar/telemax-manual/downloads/RecMAXP32_20240710_1_1_2_mnf.s37)

    - Corregido: No estaba funcionando el borrado de mandos: la indicación de pulsación larga con el led encendido sí, pero al liberar se entraba en modo aprendizaje sin hacer el borrado propiamente dicho

* [versión 12/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/RecMAXP32_20241202_1_2_1_mnf.s37)

    - Reducido tiempo de desconexión al no recibir comunicación desde el mando a 1.0 segundos, para la deshabilitación de relés rápida al perder conectividad con él

### EmiMAX32

(variante para versión de hardware con micro "EFR32")

* [versión 16/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/EmiMAX32_20241030_1_6_1_mnf.s37)

    - Periodo de envío de mensajes radio cambiado de 500 a 250 ms en operación (de 4 s a 2 s en intento de conexión)

* [versión 17/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/EmiMAX32_20241202_1_7_1_mnf.s37)

    - Aunque la opción CualquierTeclaEnciendeMando esté activa, si el mando se apagó con el botón de STOP, se requiere START para encendido (sólo se encendería con cualquier tecla tras apagado por inactividad)

    - Si no estamos en ningún estado especial de conectividad (conectado/en asociación), pero hay una tecla pulsada, se enciende el led en rojo, para que el usuario no piense que el mando no tiene batería

* [versión 18/0004](https://bitbucket.org/prodimar/telemax-manual/downloads/EmiMAX32_20250203_1_8_4_mnf.s37)

    Soluciona varios problemas de estabilidad:

    - Operación fiable con actividad radio de otros conjuntos emisor-receptor en el entorno

    - Robo de conexión de 1 mando a otro (mandos con el mismo número de serie, se entiende)

    - Bloqueo radio al encender y apagar rápidamente el mando

* [versión 19/0001](https://bitbucket.org/prodimar/telemax-manual/downloads/EmiMAX32_20250204_1_9_1_mnf.s37)

    - Parpadeo de led al ritmo de la transmisión radio, pero con una tasa máxima de 2 Hz (para diferenciarlo bien del parpadeo de 8Hz de la indicación de tecla pulsada)

    - Determinación de puerto serie conectado con múltiples observaciones, para no desactivar la Uart si nos coincide un bit a 0 durante un diálogo

    - Potencia de transmisión:

        1. A potencia máxima => máxima potencia del micro

        2. A potencia reducida => 6dB menos, para hacerlo asimilable a la potencia transmitida por el otro mando

